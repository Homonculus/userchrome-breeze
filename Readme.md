# UserChrome Breeze 
A simple Breeze theme for Mozilla Firefox.

## Installation
Just copy the `icons` forlder and `userChrome.css` file to your Firefox chrome directory.

### Dark theme
For the dark variant you need to rename `icons-dark` to `icons`.

## Screenshot 
![screenshot](screenshots/screen.png)